package it.linksmt.gamification.data;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.io.IOException;

import it.linksmt.gamification.data.model.LoggedInUser;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    interface OnRegistrationListener {
        void onRegistrationSuccess(LoggedInUser user);

        void onRegistrationFailure(Throwable t);
    }

    interface OnLoginListener {

        void onLoginSuccess(LoggedInUser user);

        void onLoginFailure(Throwable t);

    }

    private FirebaseAuth auth;

    public LoginDataSource() {
        auth = FirebaseAuth.getInstance();
    }

    public Task<AuthResult> register(String name, String password, final OnRegistrationListener callback) {
        return auth.createUserWithEmailAndPassword(name, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful() && task.getResult() != null && task.getResult().getUser() != null) {
                    LoggedInUser user = new LoggedInUser(task.getResult().getUser().getUid(), task.getResult().getUser().getDisplayName());
                    callback.onRegistrationSuccess(user);
                } else {
                    callback.onRegistrationFailure(new IOException("Registration failed"));
                }

            }
        });
    }

    public Task<AuthResult> login(String name, String password, final OnLoginListener callback) {
        return auth.signInWithEmailAndPassword(name, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful() && task.getResult() != null && task.getResult().getUser() != null) {
                    LoggedInUser user = new LoggedInUser(task.getResult().getUser().getUid(), task.getResult().getUser().getDisplayName());
                    callback.onLoginSuccess(user);
                } else {
                    callback.onLoginFailure(new IOException("Registration failed"));
                }
            }
        });
    }

//    public Result<LoggedInUser> login(String username, String password) {
//
//        try {
//            // TODO: handle loggedInUser authentication
//            LoggedInUser fakeUser =
//                    new LoggedInUser(
//                            java.util.UUID.randomUUID().toString(),
//                            "Jane Doe");
//            return new Result.Success<>(fakeUser);
//        } catch (Exception e) {
//            return new Result.Error(new IOException("Error logging in", e));
//        }
//    }

    public void logout() {
        // TODO: revoke authentication
    }
}