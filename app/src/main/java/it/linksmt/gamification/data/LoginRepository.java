package it.linksmt.gamification.data;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import it.linksmt.gamification.data.model.LoggedInUser;

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */
public class LoginRepository {

    private static volatile LoginRepository instance;

    private LoginDataSource dataSource;


    // If user credentials will be cached in local storage, it is recommended it be encrypted
    // @see https://developer.android.com/training/articles/keystore
    private LoggedInUser user = null;

    // private constructor : singleton access
    private LoginRepository(LoginDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static LoginRepository getInstance(LoginDataSource dataSource) {
        if (instance == null) {
            instance = new LoginRepository(dataSource);
        }
        return instance;
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public LoggedInUser getUser() {
        return user;
    }

    public void logout() {
        user = null;
        dataSource.logout();
    }

    private void setLoggedInUser(LoggedInUser user) {
        this.user = user;
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
    }

    public Task<AuthResult> login(String username, String password) {

        return dataSource.login(username, password, new LoginDataSource.OnLoginListener() {
            @Override
            public void onLoginSuccess(LoggedInUser user) {
                setLoggedInUser(user);
            }

            @Override
            public void onLoginFailure(Throwable t) {
                setLoggedInUser(null);
            }
        });

        // handle login
//        Result<LoggedInUser> result = dataSource.login(username, password);
//        if (result instanceof Result.Success) {
//            setLoggedInUser(((Result.Success<LoggedInUser>) result).getData());
//        }
//        return result;
    }

    public Task<AuthResult> register(String username, String password) {

        return dataSource.register(username, password, new LoginDataSource.OnRegistrationListener() {
            @Override
            public void onRegistrationSuccess(LoggedInUser user) {
                setLoggedInUser(user);
            }

            @Override
            public void onRegistrationFailure(Throwable t) {
                setLoggedInUser(null);
            }
        });

        // handle login
//        Result<LoggedInUser> result = dataSource.login(username, password);
//        if (result instanceof Result.Success) {
//            setLoggedInUser(((Result.Success<LoggedInUser>) result).getData());
//        }
//        return result;
    }
}